var myInput  = document.forms['myinput'];
var tableElem = document.getElementById("mytable");
var tableAddColumn = document.getElementById("addColumn");
var tableAddRow = document.getElementById("addRow");


function  inputText(elem){
    elem.innerHTML=this.value;
}

tableElem.onclick = function(event){
    var input = document.createElement("input");
    var thisElement = event.target;
    thisElement.innerHTML="";
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'text');
    thisElement.append(input);
    console.log(thisElement);
    input.addEventListener('blur', function() {
        thisElement.innerHTML=this.value;
        this.remove();
    });
};

tableAddRow.onclick = function(event){
    var countCells = tableElem.rows[0].cells.length;
    var newTr = document.createElement('tr');
    for (var i=0; i< countCells; i++){
        var newTd = document.createElement('td');
        newTr.appendChild(newTd);
    }
    tableElem.appendChild(newTr)

}

tableAddColumn.onclick = function(event){
    for (var i =0; i<tableElem.rows.length; i++){
        var newTd = document.createElement('td');
    tableElem.rows[i].appendChild(newTd);
    }
}